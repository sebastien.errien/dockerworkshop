# Ports

Les conteneurs peuvent exposer des ports pour êtres accédés.

Pour illustrer cela, on va utiliser l'image [NodeJs](#https://hub.docker.com/_/node). 
Le but est d'exécuter l'application d'exemple [app.js](#../exemples/node_server/app.js).

Récupérer le code de l'appli et se placer dans le répertoire contenant ces sources.

```
docker run -d --rm -v $(pwd):/usr/src/app -w /usr/src/app -p 3000:3000 --name samplenode node app.js
```

- `-d` pour un lancement en mode détaché (en tâche de fond).
et la main est rendu à l'utilisateur.
- `--rm` pour supprimer le conteneur quand il est stoppé.
- `-p 3000:3000` pour exposer le port 3000 du conteneur sur le port 3000 de l'hôte.
- `--name hello` pour choisir le nom du conteneur.
- `-w /usr/src/app` pour faire du répertoire `/usr/src/app` du conteneur le répertoire de travail.
- `-v $(pwd):/usr/src/app` pour mapper le répertoire courant (renvoyé par la commande `$PWD`) sur le répertoire `/usr/src/app`.

Visualisation dans le navigateur :

![image](../images/nodesample.png)

Pour stopper le conteneur

```
docker stop samplenode
```