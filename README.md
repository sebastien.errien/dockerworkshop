# Workshop Docker
# Prérequis

* [Docker](https://docs.docker.com/engine/install/) (Docker server pour linux, Docker Desktop pour Windows et Mac Os)


# Sommaire

[[_TOC_]]


## Qu'est-ce que Docker

[Docker](https://www.docker.com/) est un outil open-source qui permet le déploiement d'une 
application à l'intérieur d'un conteneur de logiciel. La technologie Docker utilise le noyau Linux 
et des fonctions de ce noyau, telles que les **groupes de contrôle cgroups**, les **espaces de noms** et un stockage indépendant **overlay2**, 
pour séparer les processus afin qu'ils puissent s'exécuter de façon indépendante.

La façon la plus simple de comprendre l'idée derrière Docker est de 
le comparer à des conteneurs d'expédition standard.

À l'époque, les sociétés de transport étaient confrontées aux défis suivants :

* Comment transporter différents types de marchandises incompatibles côtes à côtes.
* Comment manipuler des colis de différentes tailles en utilisant le même véhicule.

Avec l'introduction des conteneurs, les briques pouvaient être posées sur du verre, 
et les produits chimiques pouvaient être stockés à côté des aliments. 

Des cargaisons de différentes tailles peuvent être placées dans un conteneur standardisé et 
chargées/déchargées par le même véhicule.

<br>

![image](./images/shipping.jpeg)

<br>

Revenons aux conteneurs dans le développement de logiciels.

Lorsqu'on développe une application, on doit fournir le code ainsi que toutes 
les dépendances possibles comme les librairies, le serveur web, éventuellement 
la base de données, etc. On peut se retrouver dans une situation où 
l'application fonctionne en local, mais ne démarre même pas sur le serveur 
de recette, ni sur la machine d'un autre développeur.  
Les causes usuelles sont les versions de 
librairies systèmes nécessaires incompatibles avec celles nécessaires pour une 
autre application tournant sur la même machine ou des ports déjà utilisés par 
exemple.

Ce problème peut être résolu en isolant l'application pour la rendre indépendante 
du système.

<br>

![image](./images/softwareshipping.jpeg)

<br>

## Différences avec la virtualisation

Traditionnellement, les machines virtuelles étaient utilisées pour isoler les applications. 
Le principal problème avec les VM est qu'un "OS supplémentaire" au-dessus du système 
d'exploitation hôte ajoute des gigaoctets d'espace au projet.  
La plupart du temps, votre serveur hébergera plusieurs VMs qui prendront encore plus de place. Un autre inconvénient 
important des machines virtuelles est la lenteur du démarrage, mais cet inconvénient sera 
probablement supprimé avec l'utilisation de micro VMs.

<br>

![image](./images/containers-vs-virtual-machines.png)

<br>

Docker élimine tous ces inconvénients en partageant le noyau du système 
d'exploitation entre tous les conteneurs fonctionnant comme des processus distincts 
du système d'exploitation hôte.  
Le système de fichiers contenu dans les conteneurs est composé de couches ("onion" FileSystem) identiques
que les conteneurs peuvent se partager pour limiter l'espace disque nécessaire.  

Exemple: deux conteneurs basés sur l'image ubuntu:18.04 tournant sur le même hôte ne nécessiteront qu'une fois
la couche ubuntu:18.04 et (au moins) une couche pour leur code respectif.

Docker n'est pas la première ni la seule plateforme de conteneurisation. Cependant, Docker 
est actuellement le plus répandu sur le marché.

## Pourquoi Docker

* La légèreté
* Un processus de développement plus rapide
* La portabilité
* Monitoring facile et clair
* Scalabilité horizontale facile

### La légèreté
    
Contrairement à la virtualisation classique, Docker exploite et partage le kernel du système d’exploitation 
de l’hôte, ce qui le rend très efficace en terme d’utilisation des ressources du système.  
Le démarrage de conteneurs est aussi très rapide puisqu'il s'agit de lancer un processus un fois le système de fichier présent.

### Un processus de développement plus rapide

Il n'y a plus besoin d'installer les briques logicielles dont dépend l'application sur le poste.
Les dépendances (PostgreSQL, Redis, MySQL, Elasticsearch, ...) peuvent être directement exécutées dans un conteneur.

Docker permet la configuration d’un système d’exploitation différent et des versions de logiciels différentes 
pour chaque conteneur. Ainsi il est possible d’installer deux logiciels incompatibles sur la même machine 
ou même de faire fonctionner deux versions différentes du même logiciel en même temps sans avoir à gérer les 
problèmes de dépendances.

### La portabilité

Les applications sont packagées avec leur runtime et les 
librairies système nécessaires. Il est possible de déployer un conteneur facilement quelle que soit la cible 
(machine local, on premise, cloud).

Toutes les applications peuvent être conteneurisées. 

### Monitoring facile et clair

Conformément au principe "Treat logs as event streams" des [Twelves Factors](https://12factor.net/), 

Docker dispose d'une méthode unifiée pour lire les fichiers de log de tous les conteneurs en cours 
d'utilisation. Plus besoin de se souvenir de tous les chemins spécifiques où l'application 
et ses dépendances stockent les fichiers de logs et d'écrire des hooks personnalisés pour gérer cela.

### Scalabilité horizontale

De part sa conception, Docker encourage les principes des [Twelves Factors](https://12factor.net/) tels que la 
communication par ports, l'usage des variables d'environnement pour la configuration. Associé au fait que la 
duplication de conteneurs est très facile, il est aisé de scaler horizontalement une application conteneurisée. 

## Pour qui Docker est-il bénéfique ?

### Pour les développeurs

En utilisant Docker les développeurs sont donc sûrs que leur application fonctionnera indépendamment du 
système d’exploitation et de l’environnement auxquels elle est soumise.
   
Ainsi ils peuvent se concentrer sur la production de code plutôt que de passer du temps à penser au système 
sur lequel l’application fonctionnera.
   
### Pour les administrateurs systèmes

Docker permet d’installer et de démarrer des conteneurs qui fonctionnent ensemble.  
Combiné avec d'autres outils, il est possible de déployer toute une application et ses dépendances avec une 
seule commande.     
Enfin les installations de mises à jour peuvent être simplifiées par une configuration facile à implémenter.

### Pour les financiers

L'utilisation des resources étant mieux exploitée, cela permet des économies notables sur les coûts 
d'hébergement.

## Terminologie

Cette section liste les termes et les définitions que vous devez connaître avant d’explorer Docker de manière 
plus approfondie. Pour obtenir d’autres définitions, consultez le [glossaire](https://docs.docker.com/glossary/) 
complet fourni par Docker.

* **Image :** : package de toutes les dépendances et informations nécessaires pour créer un 
conteneur. Une image inclut toutes les dépendances (notamment les frameworks) ainsi que la configuration de 
déploiement et d’exécution à utiliser par le runtime du conteneur. En règle générale, une image est dérivée 
de plusieurs images de base qui sont empilées en couches pour former le système de fichiers du conteneur. 
Une image est immuable une fois qu’elle a été créée.

* **Dockerfile :** fichier texte qui contient des instructions pour la génération d’une image. C’est comme 
un script de commandes, la première ligne indique l’image de base avec laquelle commencer, puis suivent les 
instructions pour installer les programmes requis, copier les fichiers, etc. jusqu’à obtenir l’environnement 
de travail requis. Une image est "buildée" à partir d'un Dockerfile avec la commande `docker build`. Depuis 
Docker 17.05, un mécanisme de **build en plusieurs étapes** permet de réduire la taille des images finales.

* **Conteneur (Container) :** instance d’une image Docker. Un conteneur représente l’exécution d’une 
application, d’un processus ou d’un service. Il renferme une image Docker, un environnement d’exécution et 
un ensemble standard d’instructions. Pour mettre un service à l’échelle, vous créez plusieurs instances 
d’un conteneur à partir de la même image.

* **Dépôt (Repository) :** collection d’images Docker, identifiées par une balise (tag) qui indique la version 
de chaque image.

* **Registre (Registry) :** service qui fournit l’accès aux dépôts. Le registre par défaut utilisé pour la plupart des 
images publiques est [Docker Hub](https://hub.docker.com/) (propriété de l’organisation Docker). Un registre contient 
généralement des dépôts de plusieurs équipes. Les entreprises utilisent souvent des registres privés pour stocker et 
gérer les images qu’elles ont créées. [Elastic Container Registry (ECR)](https://aws.amazon.com/fr/ecr/) d'AWS est un 
autre exemple de registre.

## Travaux pratiques

* [Helloworld](helloworld/README.md)
* [Volumes](volumes/README.md)
* [Variables d'environnement](env_variables/README.md)
* [Gestion des ports](ports/README.md)
* [Build d'une image](build/README.md)
* [Lancement d'une application multi conteneurs](compose/README.md)

## Limites à l'utilisation de Docker

Docker est une technologie très efficace pour la gestion de conteneurs uniques. Cependant, à mesure 
qu'augmente le nombre de conteneurs et d'applications conteneurisées (tous décomposés en plusieurs 
composants), la gestion et l'orchestration se complexifient. Finalement, il faut prendre du recul et 
regrouper plusieurs conteneurs pour assurer la distribution des services (réseau, sécurité, télémétrie, 
etc.) vers tous vos conteneurs. C'est précisément à ce niveau qu'interviennent les orchestrateurs et 
notamment Kubernetes.

L'orchestrateur est un outil qui simplifie la gestion des clusters et des hôtes. Les orchestrateurs vous permettent 
de gérer les images, les conteneurs et les hôtes à l’aide d’une interface de ligne de commande (CLI) ou d’une 
interface graphique utilisateur. Vous pouvez gérer la mise en réseau des conteneurs, les configurations, l’équilibrage 
de charge, la découverte des services, la haute disponibilité, la configuration des hôtes Docker, et bien plus encore.  
Un orchestrateur gère l’exécution, la distribution, la mise à l’échelle et la réparation des charges de travail dans 
une collection de serveurs.

## Utilisation de Docker chez Cbp

### Avantages

- Les équipes produits sont complètement autonomes pour déployer des applications, aucune intervention manuelle n’est nécessaire.
- Tous les runtimes sont supportés, soit par mise en place de solutions par l’équipe DTD-Plateformes, soit par l’utilisation par les équipes produits d’images docker comme livrables. Cela nécessite néanmoins des compétences supplémentaires au sein des équipes Produits et de revoir la matrice de responsabilité, notamment sur le maintien en condition de sécurité de chaque runtime. 
- Lorsqu’une application est indisponible, elle est remplacée automatiquement sans arrêt de service (comme dans le PaaS v2, pour les applications qui le supportent).  
- La gestion des ressources est plus efficace, permettant d’utiliser moins de serveurs que les précédentes solutions pour un même nombre d’applications. Il y a aussi la possibilité de réserver le nombre d’instances minimum du cluster pour des durées de 1 à 3 ans, ce qui en réduit le coût d’usage.  
- La gestion des règles d’accès positionnée au niveau des serveurs apporte plus de flexibilité.  
- La gestion des règles d’accès positionnée au niveau des serveurs apporte plus de sécurité.  
- Les temps de déploiement des applications est court.  
- Déploiements d’applications sans interruption de service (comme dans le PaaS v2, pour les applications qui le supportent).  
La gestion des droits d’accès réseaux au niveau du conteneur est une avancée en terme de sécurité par rapport au trusted network.  

Cette solution est portable sur tous les fournisseurs (cloud et infogérance). En ajoutant à cela la possibilité de fédérer des clusters chez des hébergeurs différents (y compris en interne), nous avons ainsi une indépendance vis à vis du cloud provider et du MSP. 

![image](./images/platformv3.png)

### Inconvénients

- La complexité de la solution est plus élevée. En effet, il y a une couche d'abstraction supplémentaire et l’ajout d’une API, d’agents sur les serveurs et d’un orchestrateur.
- Les briques techniques ajoutées doivent êtres monitorées.  
- La gestion des règles d’accès positionnée au niveau des serveurs nécessite que cette configuration soit entièrement automatisée.  
- Nous perdons la possibilité de bénéficier d’une solution entièrement managée.  
- Le suivi des coûts par application devient très compliqué.  
- La supervision mise en place avec Linkbynet est à revoir.  
- Il existe un risque que certaines de nos applications ne soient pas éligibles à ce type d’hébergement sans une refonte profonde.  
- L’autonomie des équipes produit favorise un time to market réduit, cependant cela transfère la charge de travail de l’équipe Plateformes vers les équipes produit.  

### Technologies utilisées

* **Kubernetes** : Kubernetes est un système open-source pour automatiser le déploiement, la mise à l’échelle et la gestion des applications conteneurisées.
* **Istio** : Istio est un maillage de services (service mesh) qui assure la gestion du trafic, l’application des politiques de sécurité et la collecte de mesures.
* **External-DNS** : External-Dns synchronise les services et les entrées Kubernetes exposés avec les fournisseurs de DNS.
* **Cert-Manager** : Cert-Manager fournit des “certificats en tant que service” aux développeurs travaillant au sein du cluster Kubernetes. Il apporte notamment la prise en charge d’ACME (Let’s Encrypt), de HashiCorp Vault, de Venafi, des autorités de certification autosignée et interne. Il est extensible pour prendre en charge les Autorités de Certification personnalisées, internes ou non prises en charge.

![image](./images/vue_ensemble_cbp.png)

### Gestion des accès

![image](./images/platformv3-acces.png)

### Vue multi-sites

![image](./images/platform-multisite-view.png)

