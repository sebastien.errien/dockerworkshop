# Helloworld

## Lancement d'un conteneur :

```
docker run ubuntu /bin/echo 'Hello world'
```

Sortie console :

```
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
d72e567cc804: Pull complete
0f3630e5ff08: Pull complete
b6a83d81d1f4: Pull complete
Digest: sha256:bc2f7250f69267c9c6b66d7b6a81a54d3878bb85f1ebb5f951c896d13e6ba537
Status: Downloaded newer image for ubuntu:latest
Hello world
```

## Lister les conteneurs

```
docker ps

CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```


```
docker ps -a

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
c1df50b0a769        ubuntu              "/bin/echo 'Hello wo…"   10 minutes ago      Exited (0) 10 minutes ago                       hungry_mayer
```

## Suppression du conteneur

```
docker rm c1df50b0a769

CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

Il n'est pas obligatoire de spécifier l'identifiant en entier. On peut ne fournir que les premiers 
caractères différenciants par exemple `docker rm c1d`

## Accéder aux logs d'un conteneur

Lancement d'un conteneur 

```
docker run -d --name hello ubuntu /bin/sh -c "while true; do echo hello world; sleep 1; done" 
```

- `-d` pour un lancement en mode détaché (en tâche de fond).
- `--name hello` pour choisir le nom du conteneur.

Accès aux logs

```
docker logs hello

hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
```

## Accéder à un conteneur

```
docker exec -ti hello /bin/sh                                  
# ls
bin  boot  dev	etc  home  lib	lib32  lib64  libx32  media  mnt  opt  proc  root  run	sbin  srv  sys	tmp  usr  var
# exit
```

- `-ti` pour attacher la console au terminal du conteneur (pseudo terminal + mode interactif).

