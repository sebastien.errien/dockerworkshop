# Build d'une image

## Principes

Pour construire une image, Docker télécharge une image qui servira de base, 
puis exécute des commandes. Chaque commande ajoute un couche (layer) et crée une image 
intermédiaire. L'identifiant d'une image intermédiaire sert de base pour la commande suivante.

L'identifiant correspondant à l'image créée par la dernière commande est l'identifiant de l'image 
finale.

Les commandes sont décrites dans un fichier de configuration appelé `Dockerfile`.

**Note** : Docker utilise un cache pour éviter de refaire une étape qu’il aurait déjà faite.

Une image devient un conteneur lorsqu'on exécute la commade `docker run <mon_image>`.

![image](../images/dockerlayer.png)

## Les commandes de base

* **FROM** : Select l'image de base à utiliser.
* **LABEL maintainer** : Optionnel, ce champ permet de documenter le mainteneur de l'image.
* **RUN** : Spécifiez les commandes permettant d'apporter des modifications à votre image et,
par la suite, aux conteneurs à partir de cette image. Cela inclut la mise à jour des paquets,
l'installation de logiciels, l'ajout d'utilisateurs, la création d'une base de données 
initiale, la mise en place de certificats, etc. Il s'agit des commandes que vous exécuteriez 
en ligne de commande pour installer et configurer votre application. Il s'agit de l'une des 
plus importantes directives du dockerfile.
* **COPY** : Définit les fichiers à copier du système de fichiers de l'hôte sur le conteneur.
* **ADD** : Comme `COPY` définit les fichiers à copier du système de fichiers de l'hôte sur le
conteneur mais il prend également en charge deux autres sources. Premièrement, vous pouvez 
utiliser une URL au lieu d'un fichier/répertoire local. Deuxièmement, vous pouvez extraire 
un fichier tar de la source directement dans la destination.
* **EXPOSE** : Définit les ports exposés par le conteneur.
* **USER** : Utilisateur par défaut : toutes les commandes seront exécutées 
 avec cet utilisateur. Il peut s'agir d'un UID ou d'un nom d'utilisateur.
* **WORKDIR** : Définit le répertoire de travail par défaut pour la commande définie dans les 
instructions `ENTRYPOINT` ou `CMD`.
* **ENTRYPOINT** : Définit le programme et les arguments exécuté au démarrage du conteneur.
* **CMD** : Définit les arguments à la commande `ENTRYPOINT`.

### Intéraction entre CMD et ENTRYPOINT

![image](../images/cmd-pwd-interact.png)

**Note** : Si `CMD` est définit à partir de l'image de base, le paramètre `ENTRYPOINT` réinitialisera 
la CMD à une valeur vide. Dans ce scénario, la `CMD` doit être définit dans l'image courante pour 
avoir une valeur.

**Note** : Si l'utilisateur spécifie des arguments pour l'exécution du `docker run`, ils remplaceront 
la valeur par défaut spécifiée dans le `CMD`.

## Exemple de Dockerfile

```
FROM openjdk:7
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
RUN javac Main.java
CMD ["java", "Main"]
```

On construit l'image avec la commande `docker build -t my-java-app .`

# Construction de l'image de notre application NodeJS

### Dockerfile

```
FROM node

WORKDIR /usr/src/ap

# Bundle app source
COPY . .

EXPOSE 3000

CMD [ "node", "app.js" ]
```

 ### Vérification que l'image est bien construite

```
docker image ls

REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
my-node-app         latest              cc2d832756b0        About a minute ago   941MB
```

### Lancement du conteneur

```
docker run --rm -p 3000:3000 --name node-ap my-node-app

Server running at http://0.0.0.0:3000/
```

![image](../images/nodesample.png)

