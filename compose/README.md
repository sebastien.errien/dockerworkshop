# Docker compose

Compose est un outil permettant de définir et d'exécuter des applications Docker 
multi-conteneurs. Avec Compose, vous utilisez un fichier YAML pour configurer les 
services de votre application. Ensuite, avec une seule commande, vous créez et 
démarrez tous les services à partir de votre configuration.

## Déploiement d'un Wordpress

### Fichier de configuration compose
```
version: '3.3'

services:
  db:
    image: mysql:5.7
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
volumes:
  db_data: {}
```

### Lancement de l'application

Depuis le répertoire contenant le fichier de configuration :

```
docker-compose up -d

Creating network "compose_default" with the default driver
Creating volume "compose_db_data" with default driver
Pulling db (mysql:5.7)...
5.7: Pulling from library/mysql
bb79b6b2107f: Pull complete
49e22f6fb9f7: Pull complete
842b1255668c: Pull complete
9f48d1f43000: Pull complete
c693f0615bce: Pull complete
8a621b9dbed2: Pull complete
0807d32aef13: Pull complete
f15d42f48bd9: Pull complete
098ceecc0c8d: Pull complete
b6fead9737bc: Pull complete
351d223d3d76: Pull complete
Digest: sha256:4d2b34e99c14edb99cdd95ddad4d9aa7ea3f2c4405ff0c3509a29dc40bcb10ef
Status: Downloaded newer image for mysql:5.7
Pulling wordpress (wordpress:latest)...
latest: Pulling from library/wordpress
bb79b6b2107f: Already exists
80f7a64e4b25: Pull complete
da391f3e81f0: Pull complete
8199ae3052e1: Pull complete
284fd0f314b2: Pull complete
f38db365cd8a: Pull complete
1416a501db13: Pull complete
ab2461348ce4: Pull complete
085f6c1f7281: Pull complete
d5b4ad1cc063: Pull complete
791ee5a4264d: Pull complete
b3544511e544: Pull complete
abf41ac6b39b: Pull complete
b6439c866cc6: Pull complete
2990e7a8854f: Pull complete
14660d5f95a2: Pull complete
b8b0371e52ce: Pull complete
14b4eaf6e3f6: Pull complete
36bf819b74a1: Pull complete
1dcde27b5fe8: Pull complete
Digest: sha256:20bffad04c9c3e696b3c6fbc48d769c5948718b57af8c9457d9a0f28b5066b4b
Status: Downloaded newer image for wordpress:latest
Creating compose_db_1 ... done
Creating compose_wordpress_1 ... done
```

L'option `-d` lance l'application en mode détaché.

### Inspection du volume

```
docker inspect compose_db_data

[
    {
        "CreatedAt": "2020-11-17T13:21:01Z",
        "Driver": "local",
        "Labels": {
            "com.docker.compose.project": "compose",
            "com.docker.compose.version": "1.27.4",
            "com.docker.compose.volume": "db_data"
        },
        "Mountpoint": "/var/lib/docker/volumes/compose_db_data/_data",
        "Name": "compose_db_data",
        "Options": null,
        "Scope": "local"
    }
]
```

### Accés à l'application

![image](../images/wordpress.png)

### Arrêt de l'application

```
docker-compose down --volumes


```