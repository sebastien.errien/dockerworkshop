# Volumes

Volumes depuis la machine hôte

![image](../images/mounts-volume.png)

Volumes partagés entre plusieurs hôtes (S3, nfs, ...)

![image](../images/volumes-shared-storage.svg)

## Partage de volume entre l'hôte et le conteneur

L'utilisation du flag `-v` permet de monter un répertoire de l'hôte dans un conteneur.

```
docker run -ti --rm -v $(pwd):/ser ubuntu

root@be0b3ad0e6be:/# ls ser/

404.html  Gemfile.lock  README.MD    _includes  _pages  _sass  assets       feed.xml
Gemfile   LICENSE.txt   _config.yml  _layouts   _posts  _site  favicon.ico  index.html

root@be0b3ad0e6be:/# exit
```

## Création d'un volume indépendant

### Création d'un volume

```
docker volume create --name DataVolume1
```

```
docker volume ls

DRIVER              VOLUME NAME
local               DataVolume1
```

### Montage du volume dans un conteneur
 
```
docker run -ti --rm -v DataVolume1:/datavolume1 ubuntu
```

### Ecriture de données sur le volume monté
```
root@a322604e0f62:/# echo "Hello world!" > /datavolume1/Example1.txt
```

Comme nous avons utilisé le flag `--rm, le conteneur sera automatiquement supprimé à la sortie de celui-ci. 
Le volume, cependant, restera accessible.

```
root@a322604e0f62:/# exit
```

### Inspection du volume

```
docker volume inspect DataVolume1

[
    {
        "CreatedAt": "2020-11-09T13:52:00Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/DataVolume1/_data",
        "Name": "DataVolume1",
        "Options": {},
        "Scope": "local"
    }
]
```

**Note** : On peut même regarder les données sur l'hôte au chemin indiqué comme le `Mountpoint`. 
Il faut cependant éviter de le modifier, car il peut entraîner la corruption des données 
si les applications ou les conteneurs ne sont pas conscients des changements.

### Montage du volume dans un autre conteneur

```
docker run -ti --rm -v DataVolume1:/datavolume1 ubuntu
```

### Vérification des données sur le volume monté

```
root@9a56d86f87c7:/# cat /datavolume1/Example1.txt
Hello world!
```

**Note** : Le flag `-v` est très flexible. Si le premier argument commence par un `/` ou un `~/`, 
il crée un montage en mode "bindmount". Sans ces caractères et il crée le volume.
  
*  `-v /path:/path/in/conteneur` **monte le répertoire hôte**, `/path` au niveau du `/path/in/conteneur`
*  `-v path:/path/in/container` **crée un volume** nommé `path` sans relation avec l'hôte.
  
## Création d'un volume avec le contenu d'un répertoire du conteneur

```
docker run -ti --rm -v DataVolume2:/var ubuntu

root@4089b7eb7fbf:/# exit
```

```
docker run --rm -v DataVolume2:/datavolume2 ubuntu ls datavolume2

backups
cache
lib
local
lock
log
mail
opt
run
spool
tmp
```

## Partager un volume entre plusieurs conteneurs

### Creation du premier conteneur

```
docker run -ti --name=Container3 -v DataVolume3:/datavolume3 ubuntu
```

Ajout de données

```
root@c3d7166e9f36:/# echo "This file is shared between containers" > /datavolume3/Example.txt
```

Sortie du conteneur

```
root@c3d7166e9f36:/# exit
```

Création du second conteneur

```
docker run -ti --name=Container4 --volumes-from Container3 ubuntu
```
```
root@2cbbaa64f1fe:/# cat /datavolume3/Example.txt
This file is shared between containers
```

```
root@2cbbaa64f1fe:/# echo "Both containers can write to DataVolume4" >> /datavolume3/Example.txt
root@2cbbaa64f1fe:/# exit
```

## Vérification des données dans le premier conteneur

```
docker start -ai Container3

root@c3d7166e9f36:/# cat /datavolume3/Example.txt
This file is shared between containers
Both containers can write to DataVolume4

root@c3d7166e9f36:/# exit
```

**Note** : Docker ne s'occupe pas du verrouillage des fichiers, les applications doivent donc 
tenir compte du verrouillage des fichiers elles-mêmes. Il est possible de monter un volume 
Docker en lecture seule pour s'assurer que la corruption des données ne se produira pas 
par accident lorsqu'un conteneur nécessite un accès en lecture seule en ajoutant :ro. 

```
docker run -ti --name=Container5 --volumes-from Container3:ro ubuntu

root@bf8eadfe6edf:/# rm /datavolume3/Example.txt
rm: cannot remove '/datavolume3/Example.txt': Read-only file system

root@bf8eadfe6edf:/# exit

```